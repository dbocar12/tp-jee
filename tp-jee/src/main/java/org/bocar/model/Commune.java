package org.bocar.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "commune")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Access(AccessType.FIELD)
public class Commune {
	
	@XmlAttribute(name = "id")
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@XmlAttribute(name ="name")
	private String name;
	
	public Commune() {
		super();
	}
	
	
	public Commune(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}


	public Commune(String name) {
		this.name = name;
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Commune [id=" + id + ", name=" + name + "]";
	}
	
	

}
