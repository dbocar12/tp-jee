package org.bocar.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class Calculator {
	
	@WebMethod
	public int add(int x, int y) {
		return  x + y;
	}
	
	@WebMethod
	public String sayBonjour(String name) {
		return  "Bonjour" + name.toString();
	}
}
