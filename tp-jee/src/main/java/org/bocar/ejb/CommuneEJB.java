package org.bocar.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.bocar.model.Commune;

@Stateless
public class CommuneEJB {
	
	@PersistenceContext(unitName = "tp-javaee")
	private EntityManager em;
	
	public long create(Commune commune) {
		em.persist(commune);
		return commune.getId();
	}
	
	public Commune findById(long id) {
		return em.find(Commune.class,id);
	}
	
	public long update(Commune commune) {
		em.merge(commune);
		return commune.getId();
	}
	
	public boolean delete(long id) {
		Commune commune = em.find(Commune.class,id);
		if(commune != null) {
			em.remove(commune);
			return true;
		} else {
			return false;
		}
	}
}
