package org.bocar.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Application;

@Path("calcul")
public class CalculatorRS{
	
	@GET 
	@Path("add/{a}/{b}")
	public int add(
			@PathParam("a") int x,
			@PathParam("b") int y
			) {
		
		return x+y;
	}
	
	@GET
	@Path("bonjour/{a}")
	public String sayBonjour(@PathParam("a") String annee) {
		return "Bonjour " + annee.toString()+ " !";
	}
	
}
