package org.bocar.rest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.bocar.ejb.CommuneEJB;
import org.bocar.model.Commune;

@Path("commune")
public class CommuneRS {
	
	 @EJB
	 private CommuneEJB communeEJB;
	 
	 @GET @Path("{id}")
	 @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
	 public Response findById(@PathParam("id") long id) {
		
		 Commune commune = communeEJB.findById(id);
		 if(commune !=null) {
			 return Response.ok(commune).build();
		 } else {
			return Response
					.status(Status.NOT_FOUND.getStatusCode(), "Commune " + id + " non trouv�e ")
					.build();
		 }
	 }
	 
	 @POST @Path("create")
	 @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	 public Response create(@FormParam("name") String name) {
		 
		 Commune commune = new Commune(name);
		 long id = communeEJB.create(commune);
		 return Response.ok(id).build();
	 }
	 
	 @PUT @Path("{id}")
	 @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	 public Response update(@PathParam("id") long id, @FormParam("name") String name) {
		 Commune commune = communeEJB.findById(id);
		 if(commune != null) {
			 commune.setName(name);
			 communeEJB.update(commune);
			 return Response.ok(id).build();
		 } else {
			 return Response.status(Status.NOT_FOUND).build();
		 }

	 }
	 
	 @DELETE @Path("{id}")
	 public Response delete(@PathParam("id") long id) {
		 if(communeEJB.delete(id)) {
			 return Response.ok(id).build();
			 
		 } else {
			 return Response.status(Status.NOT_FOUND).build();
		 }
	 }

}
