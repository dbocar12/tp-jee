<%@page import="org.bocar.model.Commune"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>My Hello JEE</title>
</head>

<body>
	<h2><%= "Commune " %> </h2>
	
	<p> Ma commune </p>

	<% Commune commune = (Commune)request.getAttribute("commune");%>
	<p> ID = <%= commune.getId()%> </p>
	<p> Name = <%= commune.getName()%> </p>
</body>
</html>